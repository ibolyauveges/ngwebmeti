(function () {
    'use strict';

    angular
        .module('app')
        .controller('MainMenuController', MainMenuController);

    MainMenuController.$inject = ['UserService', 'HttpAsyncService','$rootScope' ,'$translate','$scope','$location'];
    function MainMenuController(UserService,HttpAsyncService, $rootScope,$translate,$scope, $location) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];
        vm.loadMainMenu = loadMainMenu;
        vm.menuClick = menuClick;
        $scope.mainMenus = []; 
        loadMainMenu();

        function loadMainMenu() {
            HttpAsyncService.httpAsyncRequest('POST', "menu.jsp", "sid="+sessionStorage.sid, onSucces, onerror);
            //onSucces("OK");
             	     	 
        }
        
        function onSucces(responseSuccess) {
                   $scope.mainMenus.push({"menucodestr": "menu_adminsetup", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_controlling", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_ds", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_ekaer_service", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_fuv_service", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_helpabout", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_labor", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_makemain", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_managerfunc", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_newdatasheets", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_prices", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_profile", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_purchase", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_sale_service", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_service", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_share", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_stkservice", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_stoservice", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_szamla", "menuicon":"glyphicon glyphicon-list-alt"});
                    $scope.mainMenus.push({"menucodestr": "menu_termekdij_services", "menuicon":"glyphicon glyphicon-list-alt"});
                    var count = 0;
                    $scope.mainMenus.forEach(function(menu){ 
                         menu.menutitle = $translate.instant(menu.menucodestr);
                         switch(count%9){
                                 case 0: menu.classname = "menu lightblue";break;            
                                 case 1: menu.classname = "menu grassgreen";break;
                                 case 2: menu.classname = "menu sunyellow";break;
                                 case 3: menu.classname = "menu purple";break;
                                 case 4: menu.classname = "menu turquoise";break;
                                 case 5: menu.classname = "menu berryred";break;
                                 case 6: menu.classname = "menu golden";break;
                                 case 7: menu.classname = "menu skyblue";break;
                                 case 8: menu.classname = "menu grey";break;
                         }
                         count++;
                    });
                    console.log($scope.mainMenus[0]);
            
        }
        
        function onerror(responseError) {
            alert("mainmenu hiba");
            $location.path("/login");
        }
        
        function menuClick(menuObj) {
            var menucodestr = menuObj.getAttribute("id");
            alert(menucodestr);
        }
    }

})();