(function () {
    'use strict';

    angular
        .module('app')
        .factory('HttpAsyncService', HttpAsyncService);

    HttpAsyncService.$inject = ['$http', '$cookies', '$rootScope', '$timeout', 'UserService'];
    function HttpAsyncService($http, $cookies, $rootScope, $timeout, UserService) {
    	console.log("HttpAsyncService begin");
        var service = {};

        service.httpAsyncRequest = httpAsyncRequest;
        
        return service;

        function httpAsyncRequest(method, url, parameters, callbackSuccess,callbackError) {
        	console.log("HttpAsyncServic.httpAsyncRequest method="+method+"url="+url+" parameters="+parameters);
            var xhttp = new XMLHttpRequest();
	        xhttp.onreadystatechange = function() {
	        if (this.readyState == 4 && this.status == 200) {
	           if(this.responseText.startsWith("OK")) {
	        	   console.log("HttpAsyncServic.httpAsyncRequest OK");
                   callbackSuccess(this.responseText);
	           	}else{
	           	  console.log("HttpAsyncServic.httpAsyncRequest ERROR");
		          callbackError(this.responseText);
	           	}
		     } else if(this.readyState == 4  && this.status != 200){
		         console.log("HttpAsyncServic.httpAsyncRequest ERROR");
	             callbackError(this.responseText);
	         }else{
	        	 console.log("HttpAsyncServic.httpAsyncRequest this.readyState="+this.readyState);
	         }
		  };
		  
          xhttp.open(method, url, true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send(parameters);
          console.log("HttpAsyncServic.httpAsyncRequest END");
        }
    }

})();