(function () {
    'use strict';

    angular
        .module('app')
        .controller('MainMenuController', MainMenuController);

    MainMenuController.$inject = ['UserService', 'HttpAsyncService','$rootScope' ,'$translate','$scope','$location'];
    function MainMenuController(UserService,HttpAsyncService, $rootScope,$translate,$scope, $location) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];
        vm.loadMainMenu = loadMainMenu;
        vm.menuClick = menuClick;
        $scope.mainMenus = []; 
        loadMainMenu();

        function loadMainMenu() {
        	console.log("loadMainMenu");
            HttpAsyncService.httpAsyncRequest('POST', "HS", "function=mainmenu_json&sessionid="+sessionStorage.sid+"&lang="+sessionStorage.lang, onSucces, onerror);
            //onSucces("OK");
             	     	 
        }
        
        function onSucces(responseSuccess) {
        	console.log("loadMainMenu OK");
                  
        	        //response: "OK jsonString"
        	        var jsonString = responseSuccess.split("@@")[1];
        	        console.log(jsonString);
        	        vm.mainMenus = JSON.parse(jsonString);
                        $scope.mainMenus=[];
			vm.mainMenus.forEach(function(menu){$scope.mainMenus.push(menu);});
			//$scope.apply();
			 $scope.$apply();
                    console.log($scope.mainMenus[0]);
            
        }
        
        function onerror(responseError) {
            alert("mainmenu hiba"+responseError);
            $location.path("/login");
        }
        
        function menuClick(menuObj) {
            var menucodestr = menuObj.getAttribute("id");
            
            alert(menucodestr);
            $scope.submenu_title = $(menucodestr).text();
            $location.path('/submenu')
            $scope.$apply;
            
        }
    }

})();
