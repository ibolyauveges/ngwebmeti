﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies','pascalprecht.translate'])
        .config(config)
        .run(run)
        
    config.$inject = ['$routeProvider', '$locationProvider','$translateProvider'];
    function config($routeProvider, $locationProvider,   $translateProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/mainmenu', {
                controller: 'MainMenuController',
                templateUrl: 'menu/mainmenu/mainmenu.view.html',
                controllerAs: 'vm'
            })
            .when('/', {
                controller: 'MainMenuController',
                templateUrl: 'menu/mainmenu/mainmenu.view.html',
                controllerAs: 'vm'
            })

            .otherwise({ redirectTo: '/login' });
        
        $translateProvider
              .useStaticFilesLoader({
                prefix: 'translations/',
                suffix: '.json'
              })
              .preferredLanguage('hu')
              .useLocalStorage()
              .useMissingTranslationHandlerLog()
            .useSanitizeValueStrategy(null);
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http','$translate'];
    function run($rootScope, $location, $cookies, $http, $translate) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }
        $rootScope.lang = navigator.language.substring(0,2);

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }
    

})();