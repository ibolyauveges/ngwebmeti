﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService', '$translate' , '$scope' ,'$timeout', '$http'];
    function LoginController($location, AuthenticationService, FlashService, $translate, $scope, $timeout, $http) {
        var vm = this;

        vm.login = login;
        vm.language = navigator.language.substring(0,2);
        vm.languages = ['hu','en','de','ro','sk','ua'];
        vm.updateLanguage = updateLanguage;
        vm.updateLanguage();
        vm.username="Papp Miklós";
        vm.password="huki8";

       
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
       
        function login() {
            vm.dataLoading = true;//Image load
            var password_sha1 = SHA1(vm.password);
            AuthenticationService.Login(vm.username, password_sha1, function (responseSuccess) {
            	   console.log("login OK");
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    sessionStorage.sid = responseSuccess.split(" ")[1];
                    console.log("----"+sessionStorage.sid+"---");
                    
                    vm.dataLoading = false;
                
                    routeMe('/mainmenu');
                    
                 }, function (responseError) {
                    alert("error "+responseError);
                    FlashService.Error(responseError.message);
                    vm.dataLoading = false;//Image hide
                });
           
        }
        
        function getLang3(){
            var lang3 = "";
        	switch (vm.language) {
                    case "hu" :  lang3 = "hun";break;
                    case "en" :  lang3 = "eng";break;
                    case "de" :  lang3 = "ger";break;
                    case "ro" :  lang3 = "rom";break;
                    case "sk" :  lang3 = "slk";break;
                    case "ua" :  lang3 = "ukr";break;
        	}
            return lang3;
        }
        
        function updateLanguage() {
            $translate.use(vm.language);
            vm.langimage = "resources/images/"+vm.language+".png";
            sessionStorage.lang = getLang3();
        }
        
        function routeMe(data) {
        	console.log("login rooting: "+data+ " begin");
            var waitForRender = function () {
                if ($http.pendingRequests.length > 0) {
                    $timeout(waitForRender);
                } else {
                    $location.path(data);
                }
            };
            $timeout(waitForRender);
            $scope.$apply();
            console.log("login rooting: "+data+" end");
        }
    }

})();
