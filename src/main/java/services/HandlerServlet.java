package services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blayer.bl_login;
import blayer.bl_law_mainservices;
import util.ConnectionProvider;
import util.MetiResponse;
import util.SessionProvider;
import utilities.QueryResponse;

public class HandlerServlet extends HttpServlet {
	private SessionProvider SP;

	public void init() throws ServletException {
		SP = SessionProvider.getInstance(this.getServletContext());
	}

	public void destroy() {
		;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Hashtable pht = new Hashtable();
		try {

			req.setCharacterEncoding("UTF-8");
			Enumeration e = req.getParameterNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String val = req.getParameter(key);
				if (val == null)
					log("---------------------------------------------------------- HandlerServlet------------- key="
							+ key + " val=" + val);
				pht.put(key, val);
			}
			if (pht.containsKey("function")) {
				try {
					if (!done_func_no_pwd(resp, pht)) {
						if (checkpwd(pht)) {
							log("after checkpwd ");
							done_functions(resp, pht);
						} else {
							log("checkpwd ELSE");
							resp.setContentType("text/html; charset=UTF-8");
							PrintWriter out = resp.getWriter();
							out.println("<html>");
							out.println("<head>");
							out.println("<title>Info</title>");
							out.println("</head>");
							out.println("<body>");
							out.println("<h1>Ervenytelen keres</h1>");
							out.println("</body>");
							out.println("</html>");
						}
					}
				} catch (Exception ex) {
					log("handlerservlet", ex);
				}
			} else {
				try {
					resp.setContentType("text/html; charset=UTF-8");
					PrintWriter out = resp.getWriter();
					out.println("<html>");
					out.println("<head>");
					out.println("<title>Login Info Version: " + SP.version + "</title>");
					out.println("</head>");
					out.println("<body>");
					out.println("<h1>Bejelentkezett felhasználók:</h1>");
					out.println(" Felhasznalok szama= " + SP.getSession_counter());
					out.println("<br>" + "Servlet verzio: " + SP.version + "<br><br>" + SP.getshortInfo());

					out.println("</body>");
					out.println("</html>");
				} catch (Exception ex) {
					log("infoservlet", ex);
				}
			}
		} catch (Exception e1) {
			log("HandlerServlet", e1);
		}
	}

	private boolean done_func_no_pwd(HttpServletResponse httpServletResponse, Hashtable pht) {
		String f = pht.get("function").toString();
		if ("echo".equals(f)) {
			try {
				String s = pht.get("s").toString();
				log("++++++++++++++echo+++++++++++++++++++++++++++++++++");
				log(s);
				log("+++++++++++++++++++++++++++++++++++++++++++++++++++");
				httpServletResponse.setContentType("text/html; charset=UTF-8");
				PrintWriter out = httpServletResponse.getWriter();

				out.println(s);

				return true;
			} catch (IOException e) {
				log("done_func_no_pwd", e);

				return true;
			}
		}
		else if (f.compareTo("getPdf") == 0) {
			/* loadPdf((String)pht.get("docid"),httpServletResponse); */
			return true;
		}
		else if (f.compareTo("getHtml") == 0) {
			/* loadHtml((String)pht.get("docid"),httpServletResponse); */
			return true;
		}

		return false;
	}

	private boolean checkpwd(Hashtable pht) {
		try {
			log(pht.toString());
			
			if (pht.containsKey("sessionid")) {
				
				String sessionid = pht.get("sessionid").toString();
				
				boolean checkedSid = SP.check_SID(sessionid);
				
				return checkedSid;
			}else{
				log("checkpwd pht doesn't contains Key sessionid");
			}
			String u = pht.get("u").toString();
			String p = pht.get("p").toString();
			ConnectionProvider PC = ConnectionProvider.getInstance(this.getServletContext());
			Long CID = new Long(PC.createConnection());
			Connection c = PC.getConnection(CID);
			boolean b = new bl_login().check_u_p(c, u, p, pht);
			PC.closeConnection(CID);
			return b;
		} catch (Exception e) {
			log("chkpwd", e);
		}
		return false;
	}

	private void done_functions(HttpServletResponse httpServletResponse, Hashtable pht) {
		String f = pht.get("function").toString();
		log("done_functions 0 function="+f);
		if ("login".equals(f)) {
			String u = pht.get("u").toString();
			done_login(httpServletResponse, pht);
			return;
		}
		else if ("mainmenu_json".equals(f)) {
			log("mainmenu_json 0");
			String sid = pht.get("sessionid").toString();
			SessionProvider SP = SessionProvider.getInstance(null);
			if (sid == null || !SP.check_SID(sid)) {
				writeErrorToResponse(httpServletResponse, "missing sessionid");
				
			}
			String JSP_userid = "" + SP.getSession(sid).userid;
			pht.put("session_userid", JSP_userid);
			if(!doneMainmenuJson(httpServletResponse, pht)){
				writeErrorToResponse(httpServletResponse, "make JSON problem");
			}
			return;
		}
		else if ("robexegyenleg".equals(f)) {
			/* gen_robexegyenleg(httpServletResponse, pht); */
			return;
		}
		else {
			writeErrorToResponse(httpServletResponse," function "+f+" doesn't exist");
		}

	}

	private void writeErrorToResponse(HttpServletResponse httpServletResponse,String cause) {
		httpServletResponse.setContentType("text/html; charset=UTF-8");
		try{
		  PrintWriter out = httpServletResponse.getWriter();
		  out.println("ERROR " + cause);
		}catch (Exception e){
			log(e.toString());
		}
	}

	private void done_login(HttpServletResponse httpServletResponse, Hashtable pht) {
		try {
			pht.put("sessioncounter", String.valueOf(SP.getSession_counter()));
			MetiResponse r = SP.startsession(pht);
			
			httpServletResponse.setContentType("text/html; charset=UTF-8");
			PrintWriter out = httpServletResponse.getWriter();
			out.print("OK " + r.getHt().get("sessionid"));

		} catch (Exception e) {
			log("done error", e);
		}
		return;
	}

	private boolean doneMainmenuJson(HttpServletResponse httpServletResponse, Hashtable pht) {
		String jsonTxt = "";
		try {
            log("doneMainmenuJson 0");
			MetiResponse r = new MetiResponse();
			bl_login loginsrv = new bl_login();

			ConnectionProvider PC = ConnectionProvider.getInstance(this.getServletContext());
			Long CID = new Long(PC.createConnection());
			Connection c = PC.getConnection(CID);
			Hashtable langht;
			QueryResponse qr = new blayer.bl_login().getlangtable(c, pht);
            if (qr != null && qr.getHt() != null) {
                langht = (Hashtable) qr.getHt();
            } else {
                langht = new Hashtable();
            }
            
			qr = loginsrv.getmenu(c, pht);
			log("doneMainmenuJson 1");
			if (qr != null && qr.isSuccess() && qr.getHt().get("menuarray") != null) {
				log("doneMainmenuJson 2");
				String[] st = (String[]) qr.getHt().get("menuarray");
				// 0:parent 1:id 2:order 3:type 4:codestr 5:iconname
				// "menucodestr": "menu_adminsetup", "menuicon":"glyphicon
				// glyphicon-list-alt", "menutitle":"","classname":""}
				
				int count = 0;
				jsonTxt = "[";
				for (int i = 0; i < st.length ; i++) {
					log("doneMainmenuJson 3 count"+count);
					String elem = st[i];
					String[] elema = elem.split(",", -6);
					String parentid = elema[0];
					if (!parentid.equals("0")) break;
					
					String codestr = elema[4];
					String menuicon = tools.I(elema[5]);
					menuicon = menuicon==null?"":menuicon;
					String menutitle = codestr;
					if (langht.get(codestr)!=null){
                        String val = (String) langht.get(codestr);
                        val = val.replace('\n', ' ');
                        menutitle = val;
					}
					
					jsonTxt += (i==0?"":",")+"{\"menuparentid\":\"" +parentid + "\",";
					jsonTxt += "\"menuid\": \"" + elema[1] + "\",";
					jsonTxt += "\"menucodestr\":\"" + elema[4] + "\",";
					
					jsonTxt += "\"menuicon\":\"" + menuicon + "\",";
					
					jsonTxt += "\"menutitle\":\"" + menutitle + "\",";
					jsonTxt += "\"menuorder\":\"" + elema[2] + "\",";
					String classname = "";
					switch (count % 9) {
					case 0:
						classname = "menu lightblue";
						break;
					case 1:
						classname = "menu grassgreen";
						break;
					case 2:
						classname = "menu sunyellow";
						break;
					case 3:
						classname = "menu purple";
						break;
					case 4:
						classname = "menu turquoise";
						break;
					case 5:
						classname = "menu berryred";
						break;
					case 6:
						classname = "menu golden";
						break;
					case 7:
						classname = "menu skyblue";
						break;
					case 8:
						classname = "menu grey";
						break;
					}
					count++;
					jsonTxt += "\"classname\":\"" + classname + "\"}";
				}
				log("doneMainmenuJson 4");
				jsonTxt += "]";
			}

			log("jsonString="+jsonTxt); 
			httpServletResponse.setContentType("text/html; charset=UTF-8");
			PrintWriter out = httpServletResponse.getWriter();
			out.print("OK@@" + jsonTxt);
			return true;
            
		} catch (Exception e) {
			log("done error", e);
		}
		return false;
	}
}
